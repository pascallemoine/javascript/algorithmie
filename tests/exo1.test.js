import {
  helloWorldResult,
  noResult,
  voidResult,
  arrayResult,
  objectResult
} from '../src/exo1';

describe('Exo1', () => {
  test('voidResult', () => {
    expect(voidResult()).toBe(undefined)
  })
  
  test('noResult', () => {
    expect(noResult()).toBe(null)
  })

  test('helloWorldResult', () => {
    expect(helloWorldResult()).toBe('Hello World !')
  })

  test('arrayResult', () => {
    expect(arrayResult()).toEqual([1, 2])
  })

  test('objectResult', () => {
    expect(objectResult()).toEqual({
      request: 'ping',
      response: 'pong'
    })
  })
})
