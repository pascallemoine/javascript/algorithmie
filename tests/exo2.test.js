import {
  upCase,
  contain,
  noWhiteSpace,
  split,
  concat,
  length,
  firstletter
} from '../src/exo2';

describe('Exo2', () => {
  test('length', () => {
    expect(length()).toBe(30)
  })

  test('stringCase', () => {
    expect(upCase()).toBe('VOICI UNE CHAINE DE CARACTÈRES EN MAJUSCULES');
  })

  test('stringContain', () => {
    expect(contain()).toBeTruthy()
  })

  test('noWhiteSpace', () => {
    expect(noWhiteSpace()).toBe('Chaine de caractères sans espace')
  })

  test('concat', () => {
    expect(concat()).toBe('John Doe')
  })

  test('split', () => {
    expect(split()).toEqual(['John', 'Doe'])
  })

  test('firstletter', () => {
    expect(firstletter()).toBe('J')
  })
})