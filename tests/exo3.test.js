import {
  firstline, addline, zeroline, include, length, sortnumber, sortobject, filter, find, map, some, every, reduce, remove
} from '../src/exo3';

describe('Exo3', () => {
  test('length', () => {
    expect(length()).toBe(3)
  })

  test('firstline', () => {
    expect(firstline()).toBe(1)
  })

  test('addline', () => {
    expect(addline()).toEqual([1, 2, 3])
  })

  test('zeroline', () => {
    expect(zeroline()).toEqual([0, 1, 2])
  })

  test('include', () => {
    expect(include()).toBeTruthy()
  })

  test('sortnumber', () => {
    expect(sortnumber()).toEqual([0, 2, 33, 90])
  })

  test('sortobject', () => {
    expect(sortobject()).toEqual([
      { nom: 'Potter', prenom: 'Harry' },
      { nom: 'Doe', prenom: 'John' },
      { nom: 'Skywalker', prenom: 'Luke' },
    ])
  })

  test('filter', () => {
    expect(filter()).toEqual([
      { nom: 'Skywalker', prenom: 'Luke' },
      { nom: 'Doe', prenom: 'John' },
    ])
  })

  test('find', () => {
    expect(find()).toEqual({ nom: 'Doe', prenom: 'John' })
  })

  test('map', () => {
    expect(map()).toEqual([10, 20, 30, 40])
  })

  test('some', () => {
    expect(some()).toBeTruthy()
  })

  test('every', () => {
    expect(every()).toBe(false)
  })

  test('reduce', () => {
    expect(reduce()).toBe(807.40)
  })

  test('remove', () => {
    expect(remove()).toEqual([
      { nom: 'Potter', prenom: 'Harry', age: 15 },
      { nom: 'Doe', prenom: 'John', age: 40 },
    ])
  })
})
