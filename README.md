# Exercices d'algorithmie javascript

## Installation du projet

Dans un terminal, se placer dans le dossier **au niveau de ce fichier**, puis exécuter la commande :

```bash
npm install
```

Cette commande va installer toutes les dépendances nécessairent à l'exécution du projet. Ceci peut prendre quelques minutes.

## Exécuter les tests des exercices

Il y a 3 exercices :

- exo1 sur les types
- exo2 sur la manipulation des strings
- exo3 sur la manipulation des tableaux

Pour exécuter la suite de tests d'un exerice, exécuter dans le terminal la commande :

```bash
npm run exo1

ou

npm run exo2, ...
```

## Les exercices

Pour valider les tests des exercices, modifier les sources des exercices dans le dossier **```src```**, en **suivant les instructions en commentaire**.
