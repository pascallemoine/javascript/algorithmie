/** ----------------------------------------------------------------------------
 * Exercice sur les chaines de caractères.
 * 
 * Aidez-vous de la documentation :
 * @see https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/String
 * -------------------------------------------------------------------------- */

/**
 * Retourner la taille de la chaine de caractères
 */
export function length() {
  const str = 'Voici une chaine de caractères'
  // votre code ici
}

/**
 * Doit retourner la variables str en majuscules
 */
export function upCase() {
  const str = 'Voici une chaine de caractères en majuscules'
  // votre code ici
}

/**
 * Doit retourner si oui ou non (true ou false) la
 * chaine str contient le mot 'Terre'
 */
export function contain() {
  const str = 'Soleil, Mars, Venus, Terre, Mercure'
  // votre code ici
}

/**
 * Doit retourner la chaine str sans les espaces en début
 * et fin de chaine
 */
export function noWhiteSpace() {
  const str = '    Chaine de caractères sans espace     '
  // votre code ici
}

/**
 * Retourner la concaténation de prenom et nom (John Doe).
 * Ne pas oublier l'espace entre les mots
 */
export function concat() {
  const prenom = 'John'
  const nom = 'Doe'
  // votre code ici
}

/**
 * Découper la chaine str par mot
 * et retourner le résultat sous forme d'une tableau à deux lignes
 */
export function split() {
  const str = 'John Doe'
  // votre code ici
}

export function firstletter() {
  const str = 'John'
  // votre code ici
}