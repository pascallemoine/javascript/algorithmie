/** ----------------------------------------------------------------------------
 * Exercices sur les tableaux.
 * 
 * Aidez-vous de la documentation mozilla :
 * @see https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array
 * -------------------------------------------------------------------------- */

/**
 * Retourner le nombre de lignes du tableau
 */
export function length() {
  const array = [1, 2, 3]
  // code ici
}

/**
 * Retourner la valeur de la première ligne du tableau array
 */
export function firstline() {
  const array = [1, 2]
  // code ici
}

/**
 * Ajouter la valeur 3 À LA FIN DU TABLEAU
 * et retourner le tableau
 */
export function addline() {
  const array = [1, 2]
  // code ici
}

/**
 * Ajouter la valeur 0 EN DÉBUT DE TABLEAU
 * et retourner le tableau
 */
export function zeroline() {
  const array = [1, 2]
  // code ici
}

/**
 * Retourner si oui ou non le tableau contient la valeur 'John'
 */
export function include() {
  const array = ['John', 'Doe']
  // code ici
}

/**
 * Retourner le tableau trié par ordre croissant
 */
export function sortnumber() {
  const array = [33, 90, 2, 0]
  // code ici
}

/**
 * Retourner le tableau trié par PRENOM des personnages
 * dans l'ordre croissant
 */
export function sortobject() {
  const array = [
    { nom: 'Potter', prenom: 'Harry' },
    { nom: 'Skywalker', prenom: 'Luke' },
    { nom: 'Doe', prenom: 'John' },
  ]
  // code ici
}

/**
 * Filtrer le tableau eFiltrer le tableaun ne conservant que les personnages
 * dont le PRENOM NE CONTIENT PAS DE 'a'.
 * Et retourner le résultat
 */
export function filter() {
  const array = [
    { nom: 'Potter', prenom: 'Harry' },
    { nom: 'Skywalker', prenom: 'Luke' },
    { nom: 'Doe', prenom: 'John' },
  ]
  // code ici
}

/**
 * Rechercher dans le tableau le personnage du nom de 'Doe'
 * Et retourner ce personnage.
 */
export function find() {
  const array = [
    { nom: 'Potter', prenom: 'Harry' },
    { nom: 'Skywalker', prenom: 'Luke' },
    { nom: 'Doe', prenom: 'John' },
  ]
  // code ici
}

/**
 * Retourner un tableau qui multiplie par 10 chaque ligne
 */
export function map() {
  const array = [1, 2, 3, 4]
  // code ici
}

/**
 * Retourner si oui ou non (true, false) le tableau
 * contient des valeurs supérieures à 10
 */
export function some() {
  const array = [33, 90, 2, 0]
  // code ici
}

/**
 * Retourner si oui ou non (true, false) tous les personnages
 * du tableau sont majeurs (age > 18 ans)
 */
export function every() {
  const array = [
    { nom: 'Potter', prenom: 'Harry', age: 15 },
    { nom: 'Skywalker', prenom: 'Luke', age: 25 },
    { nom: 'Doe', prenom: 'John', age: 40 },
  ]
  // code ici
}

/**
 * Retourner la somme des prix des produits
 */
export function reduce() {
  const array = [
    { article: 'crayon', prix: 2 },
    { article: 'cahier', prix: 5.40 },
    { article: 'ordinateur', prix: 800 }
  ]
  // code ici
}

/**
 * Supprimer Luke Skywalker du tableau
 * et retourner le tableau
 */
export function remove() {
  const array = [
    { nom: 'Potter', prenom: 'Harry', age: 15 },
    { nom: 'Skywalker', prenom: 'Luke', age: 25 },
    { nom: 'Doe', prenom: 'John', age: 40 },
  ]
  // code ici
}