/**
 * Doit retourner la valeur indéfinie
 */
export function voidResult() {
  // votre code ici
}

/**
 * Doit retourner la valeur nulle
 */
export function noResult() {
  // votre code ici
}

/**
 * Doit retourner le texte 'Hello World !'
 * en respectant la casse et la ponctuation
 */
export function helloWorldResult() {
   // votre code ici
}

/**
 * Doit retourner un tableau contenant les chiffres 1 et 2
 */
export function arrayResult() {
   // votre code ici
}

/**
 * Doit retourner un objet contenant les propriétés :
 * request: 'ping'
 * response: 'pong'
 */
export function objectResult() {
   // votre code ici
}